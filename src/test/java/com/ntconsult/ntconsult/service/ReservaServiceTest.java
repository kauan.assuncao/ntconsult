package com.ntconsult.ntconsult.service;

import com.ntconsult.ntconsult.dto.request.BuscarReservaRequest;
import com.ntconsult.ntconsult.dto.request.RegistrarReservaRequest;
import com.ntconsult.ntconsult.dto.response.RegistroReservaResponse;
import com.ntconsult.ntconsult.dto.response.ReservaResponse;
import com.ntconsult.ntconsult.exception.CheckinInvalidoExcetion;
import com.ntconsult.ntconsult.repository.ReservaRepository;
import com.ntconsult.ntconsult.repository.entity.QuartoEntity;
import com.ntconsult.ntconsult.repository.entity.ReservaEntity;
import com.ntconsult.ntconsult.repository.entity.TipoPagamento;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReservaServiceTest {

    @InjectMocks
    private ReservaService service;

    @Mock
    private ReservaRepository repository;

    @Captor
    private ArgumentCaptor<ReservaEntity> reservaCaptor;

    private Clock clock = Clock.fixed(
            Instant.parse("2024-06-01T00:00:00Z"), ZoneId.of("UTC"));

    @Test
    void deveListarReservas() {
        List<ReservaEntity> reservas = new ArrayList<>();

        QuartoEntity quarto = mock(QuartoEntity.class);
        when(quarto.getId()).thenReturn(1L);

        ReservaEntity reserva = mock(ReservaEntity.class);
        when(reserva.getId()).thenReturn(1L);
        when(reserva.getCheckin()).thenReturn(LocalDate.now(clock).minusDays(15));
        when(reserva.getCheckout()).thenReturn(LocalDate.now(clock));
        when(reserva.getQuarto()).thenReturn(quarto);
        reservas.add(reserva);

        reserva = mock(ReservaEntity.class);
        when(reserva.getId()).thenReturn(2L);
        when(reserva.getCheckin()).thenReturn(LocalDate.now(clock).minusMonths(1));
        when(reserva.getCheckout()).thenReturn(LocalDate.now(clock).plusDays(5));
        when(reserva.getQuarto()).thenReturn(quarto);
        reservas.add(reserva);

        when(repository.findAll()).thenReturn(reservas);

        List<ReservaResponse> reservasResponse = service.listar();

        assertThat(reservasResponse).isNotNull();
        assertThat(reservasResponse).hasSize(2);

        ReservaResponse response = reservasResponse.get(0);
        assertThat(response.id()).isEqualTo(1L);
        assertThat(response.checkIn()).isEqualTo(LocalDate.now(clock).minusDays(15));
        assertThat(response.checkOut()).isEqualTo(LocalDate.now(clock));
        assertThat(response.quartoId()).isEqualTo(1);

        response = reservasResponse.get(1);
        assertThat(response.checkIn()).isEqualTo(LocalDate.now(clock).minusMonths(1));
        assertThat(response.checkOut()).isEqualTo(LocalDate.now(clock).plusDays(5));
        assertThat(response.quartoId()).isEqualTo(1);
    }

    @Test
    void deveConsultarReservas() {
        List<ReservaEntity> reservas = new ArrayList<>();

        QuartoEntity quarto = mock(QuartoEntity.class);
        when(quarto.getId()).thenReturn(1L);

        ReservaEntity reserva = mock(ReservaEntity.class);
        when(reserva.getId()).thenReturn(1L);
        when(reserva.getCheckin()).thenReturn(LocalDate.now(clock).minusDays(15));
        when(reserva.getCheckout()).thenReturn(LocalDate.now(clock));
        when(reserva.getQuarto()).thenReturn(quarto);
        reservas.add(reserva);

        when(repository.findByCheckin(any())).thenReturn(reservas);

        BuscarReservaRequest request = new BuscarReservaRequest(null);
        List<ReservaResponse> reservasResponse = service.consultar(request);

        assertThat(reservasResponse).isNotNull();
        assertThat(reservasResponse).hasSize(1);

        ReservaResponse response = reservasResponse.get(0);
        assertThat(response.id()).isEqualTo(1L);
        assertThat(response.checkIn()).isEqualTo(LocalDate.now(clock).minusDays(15));
        assertThat(response.checkOut()).isEqualTo(LocalDate.now(clock));
        assertThat(response.quartoId()).isEqualTo(1);
    }

    @Test
    void deveLancarErroQuandoCheckinInvalido() {
        RegistrarReservaRequest request = new RegistrarReservaRequest(
                new RegistrarReservaRequest.Cliente(
                        null,
                        null
                ),
                new RegistrarReservaRequest.Reserva(
                        LocalDate.now(clock).minusDays(1),
                        null,
                        1
                ),
                new RegistrarReservaRequest.Pagamento(
                        null,
                        null
                )
        );

        Assertions.assertThrows(CheckinInvalidoExcetion.class, () -> service.criar(request, 1L));
        verify(repository, never()).save(any());
    }

    @Test
    void deveLancarErroQuandoCheckoutInvalido() {
        RegistrarReservaRequest request = new RegistrarReservaRequest(
                new RegistrarReservaRequest.Cliente(
                        null,
                        null
                ),
                new RegistrarReservaRequest.Reserva(
                        LocalDate.now(clock).minusDays(10),
                        LocalDate.now(clock),
                        1
                ),
                new RegistrarReservaRequest.Pagamento(
                        null,
                        null
                )
        );

        Assertions.assertThrows(CheckinInvalidoExcetion.class, () -> service.criar(request, 1L));
        verify(repository, never()).save(any());
    }

    @Test
    void deveRegistrarNovaReserva() {
        RegistrarReservaRequest.Cliente cliente = new RegistrarReservaRequest.Cliente(
                "Fulano",
                "6199999999"
        );

        RegistrarReservaRequest.Reserva reserva = new RegistrarReservaRequest.Reserva(
                LocalDate.now(clock).plusDays(10),
                LocalDate.now(clock).plusDays(30),
                1
        );

        RegistrarReservaRequest.Pagamento pagamento = new RegistrarReservaRequest.Pagamento(
                TipoPagamento.CARTAO,
                4000.0
        );

        RegistrarReservaRequest request = new RegistrarReservaRequest(
                cliente,
                reserva,
                pagamento
        );

        when(repository.save(any(ReservaEntity.class))).thenAnswer(invocation -> {
            ReservaEntity entity = invocation.getArgument(0, ReservaEntity.class);
            entity.setId(1L);
            return entity;
        });

        RegistroReservaResponse response = service.criar(request, 1L);

        verify(repository).save(reservaCaptor.capture());
        assertThat(response.reservaId()).isEqualTo(1L);

        ReservaEntity entity = reservaCaptor.getValue();
        assertThat(entity.getCheckin()).isEqualTo(reserva.checkin());
        assertThat(entity.getCheckout()).isEqualTo(reserva.checkout());
        assertThat(entity.getQuarto().getId()).isEqualTo(reserva.quartoId());

        assertThat(entity.getNome()).isEqualTo(cliente.nome());
        assertThat(entity.getContato()).isEqualTo(cliente.contato());

        assertThat(entity.getTipoPagamento()).isEqualTo(pagamento.tipoPagamento());
        assertThat(entity.getValor()).isEqualTo(pagamento.valor());
    }
}