package com.ntconsult.ntconsult.service;

import com.ntconsult.ntconsult.dto.request.BuscarHotelRequest;
import com.ntconsult.ntconsult.dto.response.HotelResponse;
import com.ntconsult.ntconsult.repository.HotelRepository;
import com.ntconsult.ntconsult.repository.entity.HotelEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HotelServiceTest {
    @InjectMocks
    private HotelService service;

    @Mock
    private HotelRepository hotelRepository;

    @Test
    void deveListarHoteis() {
        List<HotelEntity> hoteisEntities = new ArrayList<>();

        HotelEntity hotel = mock(HotelEntity.class);
        when(hotel.getNome()).thenReturn("Hotel 1");
        when(hotel.getCidade()).thenReturn("Cidade 1");
        when(hotel.getComodidades()).thenReturn(List.of("Churrasqueira", "Piscina"));
        hoteisEntities.add(hotel);

        hotel = mock(HotelEntity.class);
        when(hotel.getNome()).thenReturn("Hotel 2");
        when(hotel.getCidade()).thenReturn("Cidade 1");
        when(hotel.getComodidades()).thenReturn(List.of("Piscina"));
        hoteisEntities.add(hotel);


        when(hotelRepository.findAll()).thenReturn(hoteisEntities);

        List<HotelResponse> hoteis = service.listar();
        assertThat(hoteis).isNotNull();
        assertThat(hoteis).hasSize(2);

        HotelResponse response = hoteis.get(0);
        assertThat(response.nome()).isEqualTo("Hotel 1");
        assertThat(response.cidade()).isEqualTo("Cidade 1");
        assertThat(response.comodidades()).hasSize(2);

        response = hoteis.get(1);
        assertThat(response.nome()).isEqualTo("Hotel 2");
        assertThat(response.cidade()).isEqualTo("Cidade 1");
        assertThat(response.comodidades()).hasSize(1);
    }

    @Test
    void deveConsultarHotel() {
        List<HotelEntity> hoteisEntities = new ArrayList<>();

        HotelEntity hotel = mock(HotelEntity.class);
        when(hotel.getNome()).thenReturn("Hotel 1");
        when(hotel.getCidade()).thenReturn("Cidade 1");
        when(hotel.getComodidades()).thenReturn(List.of("Churrasqueira", "Piscina"));
        hoteisEntities.add(hotel);

        when(hotelRepository.findByNomeLikeOrCidadeLike(any(), any())).thenReturn(hoteisEntities);

        BuscarHotelRequest request = new BuscarHotelRequest("Hotel 1", null);
        List<HotelResponse> hoteis = service.consultar(request);
        assertThat(hoteis).isNotNull();
        assertThat(hoteis).hasSize(1);

        HotelResponse response = hoteis.get(0);
        assertThat(response.nome()).isEqualTo("Hotel 1");
        assertThat(response.cidade()).isEqualTo("Cidade 1");
        assertThat(response.comodidades()).hasSize(2);
    }
}