package com.ntconsult.ntconsult.service;

import com.ntconsult.ntconsult.dto.request.BuscarHotelRequest;
import com.ntconsult.ntconsult.dto.response.HotelResponse;
import com.ntconsult.ntconsult.repository.HotelRepository;
import com.ntconsult.ntconsult.transform.HotelTransform;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class HotelService {

    private final HotelRepository hotelRepository;

    public List<HotelResponse> listar() {
        return hotelRepository.findAll().stream()
                .map(HotelTransform::toHotelResponse)
                .toList();
    }

    public List<HotelResponse> consultar(BuscarHotelRequest request) {
        return hotelRepository.findByNomeLikeOrCidadeLike(request.nome(), request.cidade()).stream()
                .map(HotelTransform::toHotelResponse)
                .toList();
    }

}
