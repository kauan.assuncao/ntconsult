package com.ntconsult.ntconsult.service;

import com.ntconsult.ntconsult.dto.request.BuscarReservaRequest;
import com.ntconsult.ntconsult.dto.request.RegistrarReservaRequest;
import com.ntconsult.ntconsult.dto.response.RegistroReservaResponse;
import com.ntconsult.ntconsult.dto.response.ReservaResponse;
import com.ntconsult.ntconsult.exception.CheckinInvalidoExcetion;
import com.ntconsult.ntconsult.exception.CheckoutInvalidoException;
import com.ntconsult.ntconsult.repository.ReservaRepository;
import com.ntconsult.ntconsult.repository.entity.ReservaEntity;
import com.ntconsult.ntconsult.transform.ReservaTransform;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReservaService {

    private final ReservaRepository repository;

    public List<ReservaResponse> listar() {
        return repository.findAll().stream()
                .map(ReservaTransform::toReservaTransform)
                .toList();
    }

    public List<ReservaResponse> consultar(BuscarReservaRequest request) {
        log.info("Consultando reservas. Request:{}", request);
        return repository.findByCheckin(request.checkIn()).stream()
                .map(ReservaTransform::toReservaTransform)
                .toList();
    }

    public RegistroReservaResponse criar(RegistrarReservaRequest request, long hotelId) {
        validar(request, hotelId);
        processarPagamento();
        ReservaEntity reservaEntity = ReservaTransform.toReservaEntity(request);
        log.info("Salvando Reserva {} Quarto {}", reservaEntity.getId(), reservaEntity.getQuarto().getId());
        repository.save(reservaEntity);
        return new RegistroReservaResponse(reservaEntity.getId());
    }

    private void validar(RegistrarReservaRequest request, long hotelId) {
        System.out.println("Validar se quarto pertence ao hotelId");
        System.out.println("Validar se quarto está disponível");
        System.out.println("Validar se quarto está disponível para checkout.");

        if (request.reserva().checkin().isBefore(LocalDate.now())) {
            throw new CheckinInvalidoExcetion(request.reserva().checkin());
        }
        if (request.reserva().checkout().isBefore(request.reserva().checkin())) {
            throw new CheckoutInvalidoException(request.reserva().checkout());
        }
    }

    private void processarPagamento() {
        System.out.println("Processando Pagamento");
    }
}
