package com.ntconsult.ntconsult.controller;

import com.ntconsult.ntconsult.dto.request.BuscarHotelRequest;
import com.ntconsult.ntconsult.dto.response.HotelResponse;
import com.ntconsult.ntconsult.service.HotelService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping ("/hoteis")
public class HotelController {

    private final HotelService hotelService;

    @GetMapping
    public List<HotelResponse> listar() {
        return hotelService.listar();
    }

    @GetMapping("/consultar")
    public List<HotelResponse> consultar(BuscarHotelRequest request) {
        return hotelService.consultar(request);
    }
}
