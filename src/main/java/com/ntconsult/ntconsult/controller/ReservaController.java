package com.ntconsult.ntconsult.controller;

import com.ntconsult.ntconsult.dto.request.BuscarReservaRequest;
import com.ntconsult.ntconsult.dto.request.RegistrarReservaRequest;
import com.ntconsult.ntconsult.dto.response.HotelResponse;
import com.ntconsult.ntconsult.dto.response.RegistroReservaResponse;
import com.ntconsult.ntconsult.dto.response.ReservaResponse;
import com.ntconsult.ntconsult.service.ReservaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/reservas")
public class ReservaController {

    private final ReservaService reservaService;

    @GetMapping
    public List<ReservaResponse> listar() {
        return reservaService.listar();
    }

    @GetMapping("/consultar")
    public List<ReservaResponse> consultar(BuscarReservaRequest request) {
        return reservaService.consultar(request);
    }

    @PostMapping("/hotel/{hotelId}")
    public RegistroReservaResponse criar(@RequestBody RegistrarReservaRequest request, @PathVariable long hotelId) throws Exception {

        return reservaService.criar(request, hotelId);
    }
}
