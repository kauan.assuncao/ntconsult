package com.ntconsult.ntconsult.transform;

import com.ntconsult.ntconsult.dto.request.RegistrarReservaRequest;
import com.ntconsult.ntconsult.dto.response.ReservaResponse;
import com.ntconsult.ntconsult.repository.entity.QuartoEntity;
import com.ntconsult.ntconsult.repository.entity.ReservaEntity;

public abstract class ReservaTransform {

    public static ReservaResponse toReservaTransform(ReservaEntity entity) {
        return new ReservaResponse(
                entity.getId(),
                entity.getQuarto().getId(),
                entity.getCheckin(),
                entity.getCheckout()
        );
    }

    public static ReservaEntity toReservaEntity(RegistrarReservaRequest request) {
        ReservaEntity entity = new ReservaEntity();
        QuartoEntity quarto = new QuartoEntity();

        quarto.setId(request.reserva().quartoId());

        entity.setCheckin(request.reserva().checkin());
        entity.setCheckout(request.reserva().checkout());
        entity.setContato(request.cliente().contato());
        entity.setNome(request.cliente().nome());
        entity.setTipoPagamento(request.pagamento().tipoPagamento());
        entity.setValor(request.pagamento().valor());
        entity.setQuarto(quarto);

        return entity;
    }
}
