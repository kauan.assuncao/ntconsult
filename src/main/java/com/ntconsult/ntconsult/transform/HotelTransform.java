package com.ntconsult.ntconsult.transform;

import com.ntconsult.ntconsult.dto.response.HotelResponse;
import com.ntconsult.ntconsult.repository.entity.HotelEntity;

public abstract class HotelTransform {

    public static HotelResponse toHotelResponse(HotelEntity entity) {
        return new HotelResponse(entity.getNome(), entity.getCidade(), entity.getComodidades());
    }
}
