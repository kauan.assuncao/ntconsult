package com.ntconsult.ntconsult.exception;

import org.hibernate.annotations.Check;

import java.time.LocalDate;

public class CheckoutInvalidoException extends BusinessException {
    public CheckoutInvalidoException(LocalDate checkout) {
        super("O checkout %s nao eh valido".formatted(checkout));
    }
}
