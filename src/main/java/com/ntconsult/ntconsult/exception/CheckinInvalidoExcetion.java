package com.ntconsult.ntconsult.exception;

import java.time.LocalDate;

public class CheckinInvalidoExcetion extends BusinessException {
    public CheckinInvalidoExcetion(LocalDate checkin) {
        super("Nao eh permitido fazer checkin para a data %s".formatted(checkin));
    }
}
