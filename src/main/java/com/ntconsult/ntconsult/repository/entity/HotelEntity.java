package com.ntconsult.ntconsult.repository.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "hotel")
public class HotelEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nome;

    private String cidade;

    private List<String> comodidades;

    @OneToMany(mappedBy = "hotel")
    private List<QuartoEntity> quartos;

    @OneToMany(mappedBy = "hotel")
    private List<AvaliacaoEntity> avaliacoes;

}
