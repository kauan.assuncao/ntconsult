package com.ntconsult.ntconsult.repository.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "reserva")
public class ReservaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate checkin;

    private LocalDate checkout;

    private String nome;

    private String contato;

    private Double valor;

    @Enumerated(EnumType.STRING)
    private TipoPagamento tipoPagamento;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "quarto_id")
    private QuartoEntity quarto;

}
