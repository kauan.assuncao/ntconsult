package com.ntconsult.ntconsult.repository.entity;

public enum TipoPagamento {
    CARTAO,
    BOLETO,
    PIX
}
