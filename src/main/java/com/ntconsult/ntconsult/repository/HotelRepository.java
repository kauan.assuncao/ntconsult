package com.ntconsult.ntconsult.repository;

import com.ntconsult.ntconsult.repository.entity.HotelEntity;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotelRepository extends ListCrudRepository<HotelEntity, Long> {

    List<HotelEntity> findByNomeLikeOrCidadeLike(String nome, String cidade);
}
