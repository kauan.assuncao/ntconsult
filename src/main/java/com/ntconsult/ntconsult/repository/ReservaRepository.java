package com.ntconsult.ntconsult.repository;

import com.ntconsult.ntconsult.repository.entity.HotelEntity;
import com.ntconsult.ntconsult.repository.entity.ReservaEntity;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReservaRepository extends ListCrudRepository<ReservaEntity, Long>  {

    List<ReservaEntity> findByCheckin(LocalDate checkIn);
}
