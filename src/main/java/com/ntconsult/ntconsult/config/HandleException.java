package com.ntconsult.ntconsult.config;

import com.ntconsult.ntconsult.exception.BusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class HandleException {

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Message> handleBusinessException(BusinessException exception) {
        return ResponseEntity
                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                .body(new Message(exception.getMessage()));
    }

    public record Message(String message) {
    }
}
