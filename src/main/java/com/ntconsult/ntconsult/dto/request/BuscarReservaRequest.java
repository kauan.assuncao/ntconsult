package com.ntconsult.ntconsult.dto.request;

import java.time.LocalDate;

public record BuscarReservaRequest(LocalDate checkIn) {
}
