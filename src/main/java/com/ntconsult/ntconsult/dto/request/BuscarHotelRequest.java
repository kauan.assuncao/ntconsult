package com.ntconsult.ntconsult.dto.request;

public record BuscarHotelRequest(String nome, String cidade) {
}
