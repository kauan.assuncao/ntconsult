package com.ntconsult.ntconsult.dto.request;


import com.ntconsult.ntconsult.repository.entity.TipoPagamento;

import java.time.LocalDate;

public record RegistrarReservaRequest(
        Cliente cliente,
        Reserva reserva,
        Pagamento pagamento
) {

    public record Cliente(
            String nome,
            String contato
    ) {
    }

    public record Reserva(
            LocalDate checkin,
            LocalDate checkout,
            long quartoId
    ) {
    }

    public record Pagamento(
            TipoPagamento tipoPagamento,
            Double valor
    ) {
    }
}
