package com.ntconsult.ntconsult.dto.response;

import java.util.List;

public record HotelResponse(String nome, String cidade, List<String> comodidades) {
}
