package com.ntconsult.ntconsult.dto.response;

public record RegistroReservaResponse(long reservaId) {
}
