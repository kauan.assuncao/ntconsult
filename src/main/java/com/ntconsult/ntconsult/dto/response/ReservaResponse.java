package com.ntconsult.ntconsult.dto.response;

import java.time.LocalDate;

public record ReservaResponse(
        long id,
        long quartoId,
        LocalDate checkIn,
        LocalDate checkOut
    ) {
}
