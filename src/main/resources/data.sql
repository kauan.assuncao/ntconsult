INSERT INTO hotel (id, nome, cidade, comodidades) VALUES
    (1, 'Hotel 1', 'Brasília', 'Area Gourmet,Piscina'),
    (2, 'Hotel 2', 'Goiânia', 'Piscina,Churrasqueira');

INSERT INTO quarto (preco, numero_hospedes, hotel_id) VALUES
    (350.0, 3, 1),
    (500.0, 2, 1),
    (890.0, 2, 1),
    (1500.0, 2, 1);

INSERT INTO reserva (checkin, checkout, quarto_id) VALUES
    ('2000-01-01', '2000-01-05', 1),
    ('2000-01-01', '2000-01-05', 2),
    ('2000-01-06', '2000-01-10', 2),
    ('2000-01-01', '2000-01-05', 3),
    ('2000-01-10', '2000-01-17', 3);